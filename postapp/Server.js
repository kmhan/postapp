var app       =     require("express")();
var mysql     =     require("mysql");
var http      =     require('http').Server(app);
var io        =     require("socket.io")(http);


var pool    =    mysql.createPool({
      host              :   'localhost',
      user              :   'root',
      password          :   '',
      database          :   'post'
});

app.get("/",function(req,res){
    res.sendFile(__dirname + '/index.html');
});


io.on('connection',function(socket){  
    console.log("A user is connected");
    socket.on('status',function(status){
      add_status(status,function(res){
        if(res){
            io.emit('feed',status);
        } else {
            io.emit('error');
        }
      });
    });
	
	socket.on('delete',function(status){
      del_status(status,function(res){
        if(res){
            io.emit('del feed',status);
        } else {
            io.emit('error');
        }
      });
    });
	
});

var add_status = function (status,callback) {
    pool.getConnection(function(err,connection){
        if (err) {
          callback(false);
          return;
        }
    connection.query("INSERT INTO `post` (`s_text`) VALUES ('"+status+"')",function(err,rows){
            connection.release();
            if(!err) {
              callback(true);
            }
        });
     connection.on('error', function(err) {
              callback(false);
              return;
        });
    });
}

var del_status = function (status,callback) {
    pool.getConnection(function(err,connection){
        if (err) {
          callback(false);
          return;
        }
    connection.query("DELETE FROM `post` WHERE `status_id` =('"+status+"')",function(err,rows){
            connection.release();
            if(!err) {
              callback(true);
            }
        });
     connection.on('error', function(err) {
              callback(false);
              return;
        });
    });
}

 app.get('/getStatus',function(req,res){
    	pool.getConnection(function(err,connection){
    		if (err) {
    			connection.release();
    			return res.json({"error" : true,"message" : "Error in database."});
    		} else {
    			connection.query("SELECT * FROM `post`",function(err,rows){
    				connection.release();
    				if (err) {
    					return res.json({"error" : true,"message" : "Error!"});
    				} else {
    					res.json({"error" : false,"message" : rows});
    				}
    			});
    		}
    		connection.on('error', function(err) {
    			return res.json({"error" : true,"message" : "Error!"});
            });
    	});
    });

http.listen(3000,function(){
    console.log("Listening on 3000");
});